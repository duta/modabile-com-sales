<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
    <!-- jQuery library (served from Google) -->
    <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>-->
    <script src="/js/jquery-1.12.1.min.js"></script>
    <!-- bxSlider Javascript file -->
    <script src="/js/jquery.bxslider.min.js"></script>
    <!-- bxSlider CSS file -->
    <link href="/css/jquery.bxslider.css" rel="stylesheet" />
</head>

<body>
<ul class="bxslider">
  <li><img src="img/baby_01.jpg" width="300" height="150"></li>
  <li><img src="img/baby_02.jpg" width="300" height="150"></li>
  <li><img src="img/baby_03.jpg" width="300" height="150"></li>
  <li><img src="img/baby_04.jpg" width="300" height="150"></li>
</ul>

<script >
$(document).ready(function(){
  $('.bxslider').bxSlider();
});
</script>
</body>
</html>

