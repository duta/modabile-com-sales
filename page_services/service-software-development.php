
<!-- ======= Banner ======= -->
		<section class="p0 container-fluid banner about_banner">
			<div class="about_banner_opacity">
				<div class="container">
					<div class="banner_info_about">
						<h1>Pengembangan Software</h1>
						<ul>
							<li>Layanan</li>
							<li><i class="fa fa-angle-right"></i></li>
							<li>Pengembangan Software</li>
						</ul>
						
					</div> <!-- End Banner Info -->
				</div> <!-- End Container -->
			</div> <!-- End Banner_opacity -->
		</section> <!-- End Banner -->
<!-- ======= /Banner ======= -->
		<section class="post-content">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						
						<div class="polaroid-text">
						<h2>Pengembangan Software</h2>
								<p>Saat ini kami memiliki team Engineer yang terus menerus secara kontinyu mengembangkan Aplikasi Vessel Tracking System. Sehingga Kami siap melayani order dari Anda jika ada kebutuhan khusus Aplikasi Vessel Tracking System. <br> <br>
Karena kami mengembangkan System ini secara mandiri kami juga menerima jasa modifikasi Aplikasi Tracking System sesuai dengan pesanan pelanggan. Misalkan untuk dikembangkan bisa memantau posisi armada baik berbasis AIS Receiver, AIS Satelit, Wifi GPS dan lain sebagainya.
</p>
						</div>
					</div> <!-- End white_bg -->
				</div> <!-- End row -->
			</div> <!-- End container -->
		</section>

