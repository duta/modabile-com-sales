	<div class="service">

<!-- ======= Banner ======= -->
		<section class="p0 container-fluid banner about_banner">
			<div class="about_banner_opacity">
				<div class="container">
					<div class="banner_info_about">
						<h1>Integrasi Sistem</h1>
						<ul>
							<li>Layanan</li>
                            <li><i class="fa fa-angle-right"></i></li>
                            <li>Integrasi Sistem</li>
						</ul>
						
					</div> <!-- End Banner Info -->
				</div> <!-- End Container -->
			</div> <!-- End Banner_opacity -->
		</section> <!-- End Banner -->
<!-- ======= /Banner ======= -->
<!-- ======== Service facts ========= -->
		<section class="post-content">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="polaroid-text">
						<h2>Integrasi Sistem</h2>
								<p>Tidak menutup kemungkinan Pelanggan menghendaki Aplikasi Vessel Tracking System yang dibangun dan kembangkan harus terintegrasi dengan System perusahaan yang lain. Sehingga dibutuhkan support layanan Jasa Integresi dengan System perusahaan yang lain.</p>
						</div>
					</div> <!-- End white_bg -->
				</div> <!-- End row -->
			</div>
		</section>
<!-- ======== /Service facts ========= -->
	</div>