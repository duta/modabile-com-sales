
<!-- ======= Banner ======= -->
		<section class="p0 container-fluid banner about_banner">
			<div class="about_banner_opacity">
				<div class="container">
					<div class="banner_info_about">
						<h1>Pemeliharaan Sistem</h1>
						<ul>
							<li>Layanan</li>
							<li><i class="fa fa-angle-right"></i></li>
							<li>Pemeliharaan Sistem</li>
						</ul>
						
					</div> <!-- End Banner Info -->
				</div> <!-- End Container -->
			</div> <!-- End Banner_opacity -->
		</section> <!-- End Banner -->
<!-- ======= /Banner ======= -->
		<section class="post-content">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="polaroid-text">
						<h2>Pemeliharaan Sistem</h2>
								<p>Bagian dari layanan kami adalah memelihara dan memastikan System yang kita kembangkan dengan user berjalan dengan baik setiap saat, sehingga jika klien menghendaki kami juga melakukan perawatan Software yang kami kembangkan dan kami sudah memiliki team yang solid.</p>
						</div>
					</div> <!-- End white_bg -->
				</div> <!-- End row -->
			</div> <!-- End container -->
		</section>
