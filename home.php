<!-- ======= revolution slider section ======= -->
<?php include("slide.php");	?>
<!-- ======= /revolution slider section ======= -->


<!-- ======= Welcome section ======= -->
<?php include("welcome.php");	?>		 
<!-- ======= /Welcome section ======= -->

<!-- ======= Who We Are ======= -->
		 <!-- End We Are -->
<!-- ======= /Who We Are ======= -->

<!-- ======= Testimonial & Company Values ======= -->
		<section class="clear_fix">

		</section> <!-- testimonial -->
<!-- ======= /Testimonial & Company Values ======= -->

<!-- ======== Small business ======== -->
		
<!-- ======== /Small business ======== -->

<!-- ======== Some Facts ======== -->
	
<!-- ======== /Some Facts ======== -->

<!-- ======== Our Advisor ======== -->
		
<!-- ======== /Our Advisor ======== -->

<!-- ======== Latest News ======== -->
		
<!-- ======== /Latest News ======== -->

<!-- ======== Our Partner & Get in touch ======== -->
		<section class="container-fluid partner_touch_sec">
			<div class="container">
				<div class="row">
					<div class="our_partner col-lg-6 col-md-7">
						<h2>Klien Kami</h2>
						<p>Berikut beberapa perusahaan yang sudah mempercayakan kebutuhan Tracking Systemnya menggunakan modabile. </p>
						<ul>
							<li><a href="http://dlu.co.id/"><img src="images/c-icon1.png" alt="PT. Dharma Lautan Utama"></a></li>
							<li><a href="https://www.pelindo.co.id/"><img src="images/c-icon2.png" alt="PT. Pelindo 3"></a></li>
							
						</ul>
						<ul>
							<li><a href="http://bakamla.go.id/"><img src="images/Logo_Bakamla_RI.png" alt="Badan Keamanan Laut RI"></a></li>
							<li><a href="http://www.ipcmarineservice.co.id/"><img src="images/IPC_Marine_Service.png" alt="IPC Marine Service"></a></li>
							<li><a href="http://www.pelindomarine.com/"><img src="images/logo_pms_header.png" alt="PT. Pelindo Marine"></a></li>
						</ul>
					</div> <!-- End our_partner -->
					<div class="get_touch col-lg-6 col-md-5 col-sm-12 col-xs-12">
						<h2>Hubungi Kami</h2>
						<form action="send-email.php" method="POST" class="contact-form">
							<div class="alert alert-success" style="display:none;">
	                        </div>
							<input class="form-control name" type="text" name="name" placeholder="Your Name">
							<input class="form-control email" type="email" name="email" placeholder="Your Email">
						    <input class="form-control" type="text" name="subject" placeholder="Subject">
						    <textarea class="form-control" name="message" placeholder="Message"></textarea><br>
						    <div class="g-recaptcha" data-sitekey="6LdU5iwUAAAAAPUMNN5gDtpZu1LDOVqL4l5khOmV" data-type="image" data-theme="light"></div>
						    <button type="submit" class="submit">submit now <i class="fa fa-arrow-circle-right"></i></button>
						</form>
					</div> <!-- End get_touch -->
				</div> <!-- End Row -->
			</div> <!-- End Container -->
		</section> <!-- End container-fluid -->
<!-- ======== /Our Partner & Get in touch ======== -->

