<!-- ======= Banner ======= -->
		<section class="p0 container-fluid banner about_banner">
			<div class="about_banner_opacity">
				<div class="container">
					<div class="banner_info_about">
						<h1>Pengalaman</h1>
						<!--<ul>
							<li><a href="index.html">Home</a></li>
							<li><i class="fa fa-angle-right"></i></li>
							<li>Pengalaman</li>
						</ul>-->
						
					</div> <!-- End Banner Info -->
				</div> <!-- End Container -->
			</div> <!-- End Banner_opacity -->
		</section> <!-- End Banner -->
<!-- ======= /Banner ======= -->
<!-- ======= Latest Work ========= -->
		<section class="latest_work latest_work_two">
			<div class="container latest_work_title">
				<h2>Kerja Terbaru Kami</h2>
				<p>Pengalaman kami dalam implementasi Tracking System 
					<br><br>
					Perusahaan Pengelola Pelabuhan
					<br>
					<b>
					PT.Pelindo III ( Persero) Cabang Perak - PT.Pelindo III (Persero) Cabang Banjarmasin - PT.Pelindo III (Persero) Cabang Semarang - PT.Pelindo Marine Service - IPC Marine Service ( Anak perusahaan Pelindo II ) - BAKAMLA ( Badan Keamanan Laut ) RI - PT.Terminal Teluk Lamong
					</b>
					<br><br>
					Perusahaan Pelayaran
					<br>
					<b>
					PT.Dharma Lautan Utama - PT.Tanto Intim Line
					</b>
					<br><br>
					Perusahaan Expedisi/ Logistik
					<br>
					<b>
					PT.Kalimas - Kendaraan pribadi maupun rental kendaraan
					</b>
				</p>
			</div>
		</section> <!-- End latest_work -->
<!-- ======= /Latest Work ========= -->