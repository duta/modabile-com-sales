<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">

		<!-- For IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- For Resposive Device -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>Vessel Tracking</title>

		<!-- Favicon -->
		<link rel="apple-touch-icon" sizes="57x57" href="images/fav-icon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="images/fav-icon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="images/fav-icon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="images/fav-icon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="images/fav-icon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="images/fav-icon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="images/fav-icon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="images/fav-icon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="images/fav-icon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="images/fav-icon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="images/fav-icon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="images/fav-icon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="images/fav-icon/favicon-16x16.png">


		<!-- Bootstrap CSS -->
		<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.css" media="screen">


		<!-- Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500italic,500,700,700italic' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,300,600,700,800,900' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,700,700italic,800,300,300italic' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=PT+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

		<!-- Font Awesome -->
		<link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css">
		<!-- Stroke Gap Icon -->
		<link rel="stylesheet" href="fonts/stroke-gap/style.css">
		<!-- owl-carousel css -->
		<link rel="stylesheet" href="css/owl.carousel.css">
		<link rel="stylesheet" href="css/owl.theme.css">
		
		<!-- owl-carousel css -->
		<link rel="stylesheet" href="css/owl.carousel.css">
		<link rel="stylesheet" href="css/owl.theme.css">
		<!-- Custom Css -->
		<link rel="stylesheet" type="text/css" href="css/custom/style.css">
   		<link rel="stylesheet" type="text/css" href="css/custom/style.mif.css">
		<link rel="stylesheet" type="text/css" href="css/responsive/responsive.css">
		


		<!--[if lt IE 9]>
	   		<script src="js/html5shiv.js"></script>
		<![endif]-->



	</head>
	<body class="home">
<!-- =======Header ======= -->
		<header>
			<div class="container-fluid top_header">
				<div class="container color-white">
					<p class="float_left">Selamat Datang Di Layanan Modabile, Kami Memiliki Pengalaman Lebih Dari <?=date("Y")-2012?> Tahun</p>
					<!--<div class="float_right">
						<ul>
							<li><a href=""><i class="fa fa-facebook"></i></a></li>
							<li><a href=""><i class="fa fa-twitter"></i></a></li>
							<li><a href=""><i class="fa fa-google-plus"></i></a></li>
							<li><a href=""><i class="fa fa-linkedin"></i></a></li>
							<li>
								<div  id="search_box">
									<input id="search" type="text" placeholder="Search here">
									<button id="button" type="submit"><span class="icon icon-Search"></span></button>
								</div>
							</li>
						</ul>
					</div>-->
				</div> <!-- end container -->
			</div><!-- end top_header -->
			<div class="bottom_header top-bar-gradient">
				<div class="container clear_fix">
					<div class="float_left logo">
						<a href="index.php">
							<img src="images/logo.png" alt="MODABILE">
						</a>
					</div>
					<div class="float_right address">
						<div class="top-info">
							<div class="icon-box">
								<span class=" icon icon-Pointer"></span>							
							</div>
							<div class="content-box">
								<p>Jl. Kebraon Village BM-20<br><span>Karang Pilang - Surabaya</span></p>
							</div>
						</div>
						<div class="top-info">
							<div class="icon-box">
								<span class="separator icon icon-Phone2"></span>							
							</div>
							<div class="content-box">
								<p>+ (62) 31 766 9882 <br> + (62) 81230900090 <br><span>marketing@ptdmc.co.id</span></p>
							</div>
						</div>
						<div class="top-info">
							<div class="icon-box">
								<span class="separator icon icon-Timer"></span>
							</div>
							<div class="content-box">
								<p>Sun - Fri 08.00 - 17.00 <br><span>Sunday - Monday Closed</span></p>
							</div>
						</div>
					</div>
				</div> <!-- end container -->
			</div> <!-- end bottom_header -->
		</header> <!-- end header -->
<!-- ======= /Header ======= -->

<!-- ======= mainmenu-area section ======= -->
		<section class="mainmenu-area stricky">
			<div class="container">
				<nav class="clearfix">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header clearfix">
				      <button type="button" class="navbar-toggle collapsed">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="fa fa-th fa-2x"></span>
				      </button>
				    </div>
					<div class="nav_main_list custom-scroll-bar pull-left" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav" id="hover_slip">
							<li><a href="index.php">Home</a></li>
							<li class="arrow_down"><a href="#">Tentang Kami</a>
								<div class="sub-menu">
									<ul>
										<li><a href="https://www.youtube.com/watch?v=qrlKqqTEfiM">Profil Perusahaan</a></li>		
										<li><a href="?r=page_about_us/testimonial">Testimoni</a></li>
										<!--<li><a href="?r=page_about_us/partners">partner</a></li>-->
									</ul>
								</div>
							</li>
							<li class="arrow_down"><a href="#">Layanan</a>
								<div class="sub-menu">
									<ul>
										<li><a href="?r=page_services/service-software-development">Pengembangan Software</a></li>
										<li><a href="?r=page_services/service-maintenance-system">Pemeliharaan Sistem</a></li>
                                        <li><a href="?r=page_services/service-integrasi-sistem">Integrasi Sistem</a></li>
									</ul>
								</div>
							</li>
							<li class="arrow_down" ><a href="#">Produk</a>
								<div class="sub-menu">
									<ul>
										<li><a href="?r=page_product/product-gps-ais-satelit">Data GPS berbasis AIS Satelit</a></li>
										<li><a href="?r=page_product/product-gps-ais-receiver">Data GPS berbasis AIS RECEIVER</a></li>
										<li><a href="?r=page_product/product-gps-satelit">Data GPS berbasis Satelit</a></li>
										<li><a href="?r=page_product/product-gps-gsm">Data GPS berbasis GSM</a></li>
										<li><a href="?r=page_product/product-gps-wifi">Data GPS Berbasis WIFI</a></li>
									</ul>
								</div>
							</li>
							<!--<li class="arrow_down" ><a href="#">AIS Satelite</a>
								<div class="sub-menu">
									<ul>
										<li><a href="?r=product-info">Product Info</a></li>
										<li><a href="?r=pricing">Harga</a></li>			
									</ul>
								</div>
							</li>-->
							<li ><a href="?r=page_experience/experience">Pengalaman</a>
								<!--<div class="sub-menu">
									<ul>
										<li><a href="portfolio.php">Portfolio One</a></li>
										<li><a href="portfolio2.php">Portfolio Two</a></li>
										<li><a href="portfolio3.php">Portfolio Three</a></li>
										<li><a href="portfolio4.php">Portfolio Four</a></li>
										<li><a href="portfolio5.php">Portfolio Five</a></li>
									</ul>
								</div>-->
							</li>
							<!--<li class="arrow_down" ><a href="blog-leftside-bar.php">Blogs</a>
								<div class="sub-menu">
									<ul>
										<li><a href="blog-leftside-bar.php">Blog Left Bar</a></li>
										<li><a href="blog-rightside-bar.php">Blog Right Bar</a></li>
										<li><a href="blog-single-column.php">Blog Single Col</a></li>
										<li><a href="blog-two-column.php">Blog Two Col</a></li>
										<li><a href="blog-details.php">Blog Details</a></li>
									</ul>
								</div>
							</li>-->

                            <!--<li><a class="contact" href="http://demo.modabile.com" target="new">Demo</a></li>-->
							<li><a class="contact" href="?r=page_contact_us/contact-us">Cara Order</a></li>
						</ul>						
					</div>
					<!--<div class="find-advisor pull-right">
						<a href="http://maps.modabile.com" class="advisor ">Register</a>
					</div>-->
				</nav> <!-- End Nav -->
			</div> <!-- End Container -->
		</section>
<!-- ======= /mainmenu-area section ======= -->

<?php
$page = array(
	'kapal-request', 
	'daftar');
	$r = 'home';
if(isset($_GET['r'])){
	$r = $_GET['r'];
	//if(in_array($r, $page)){
		include_once($r.'.php');
	//}		
	//else{
	//	include("home.php");
	//}
}
else{
	include("home.php");
}
?>

<!-- ============= Footer ================ -->
		<footer>
			<div class="bottom_footer container-fluid">
				<div class="container">
					<p class="float_left">Copyright &copy; Modabile 2015. All rights reserved. </p>
					<p class="float_right">Created by: PT. Duta Media Cipta</p>
				</div>
			</div> <!-- End bottom_footer -->
		</footer>
<!-- ============= /Footer =============== -->

		<!-- Js File -->

		<!-- j Query -->
		<script type="text/javascript" src="js/jquery-2.1.4.js"></script>
		<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
		<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
		<script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
		<script src="js/revolution-slider/jquery.themepunch.tools.min.js"></script> <!-- Revolution Slider Tools -->
		<script src="js/revolution-slider/jquery.themepunch.revolution.min.js"></script> <!-- Revolution Slider -->
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.actions.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.carousel.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.kenburn.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.layeranimation.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.migration.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.navigation.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.parallax.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.slideanims.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.video.min.js"></script>

		<!-- Bootstrap JS -->
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/jquery.appear.js"></script>
		<script type="text/javascript" src="js/jquery.countTo.js"></script>
		<script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
		<!-- owl-carousel -->
		<script type="text/javascript" src="js/owl.carousel.js"></script>
		<script src="js/owl-custom.js"></script>
		<!-- Custom & Vendor js -->
		<script type="text/javascript" src="js/custom.js"></script>
		<script src='https://www.google.com/recaptcha/api.js'></script>
		<script src="http://maps.google.com/maps/api/js"></script> <!-- Gmap Helper -->
		<script src="js/gmap.js"></script> <!-- gmap JS -->
        <script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
		<script src="js/jquery.sliphover.min.js"></script>
		<script type="text/javascript" src="js/jquery.blockui.min.js"></script>
		<script type="text/javascript" src="js/jquery.validate.js"></script>
	</body>
</html>