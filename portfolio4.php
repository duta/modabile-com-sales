<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">

		<!-- For IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- For Resposive Device -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>Me-Financial</title>

		<!-- Favicon -->
		<link rel="apple-touch-icon" sizes="57x57" href="images/fav-icon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="images/fav-icon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="images/fav-icon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="images/fav-icon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="images/fav-icon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="images/fav-icon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="images/fav-icon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="images/fav-icon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="images/fav-icon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="images/fav-icon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="images/fav-icon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="images/fav-icon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="images/fav-icon/favicon-16x16.png">


		<!-- Bootstrap CSS -->
		<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.css" media="screen">


		<!-- Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500italic,500,700,700italic' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,300,600,700,800,900' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,700,700italic,800,300,300italic' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=PT+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

		<!-- Font Awesome -->
		<link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css">
		<!-- Stroke Gap Icon -->
		<link rel="stylesheet" href="fonts/stroke-gap/style.css">

		<!-- Custom Css -->
		<link rel="stylesheet" type="text/css" href="css/custom/style.css">
		<link rel="stylesheet" type="text/css" href="css/responsive/responsive.css">


		<!--[if lt IE 9]>
	   		<script src="js/html5shiv.js"></script>
		<![endif]-->



	</head>
	<body class="portfolio4">

<!-- =======Header ======= -->
		<header>
			<div class="container-fluid top_header">
				<div class="container">
					<p class="float_left">Welcome to Me Financial Services, we have over 12 years of expertise</p>
					<div class="float_right">
						<ul>
							<li><a href=""><i class="fa fa-facebook"></i></a></li>
							<li><a href=""><i class="fa fa-twitter"></i></a></li>
							<li><a href=""><i class="fa fa-google-plus"></i></a></li>
							<li><a href=""><i class="fa fa-linkedin"></i></a></li>
							<li>
								<div  id="search_box">
									<input id="search" type="text" placeholder="Search here">
									<button id="button" type="submit"><span class="icon icon-Search"></span></button>
								</div>
							</li>
						</ul>
					</div>
				</div> <!-- end container -->
			</div><!-- end top_header -->
			<div class="bottom_header top-bar-gradient">
				<div class="container clear_fix">
					<div class="float_left logo">
						<a href="index.php">
							<img src="images/logo.png" alt="Me Finance">
						</a>
					</div>
					<div class="float_right address">
						<div class="top-info">
							<div class="icon-box">
								<span class=" icon icon-Pointer"></span>							
							</div>
							<div class="content-box">
								<p> 13005 Greenville Avenue <br><span>California, TX 70240</span></p>
							</div>
						</div>
						<div class="top-info">
							<div class="icon-box">
								<span class="separator icon icon-Phone2"></span>							
							</div>
							<div class="content-box">
								<p>+ (1800) 456 7890 <br><span>info@mefinance.com</span></p>
							</div>
						</div>
						<div class="top-info">
							<div class="icon-box">
								<span class="separator icon icon-Timer"></span>
							</div>
							<div class="content-box">
								<p>Mon - Sat 9.00 - 19.00 <br><span>Sunday Closed</span></p>
							</div>
						</div>
					</div>
				</div> <!-- end container -->
			</div> <!-- end bottom_header -->
		</header> <!-- end header -->
<!-- ======= /Header ======= -->

<!-- ======= mainmenu-area section ======= -->
		<section class="mainmenu-area stricky">
			<div class="container">
				<nav class="clearfix">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header clearfix">
				      <button type="button" class="navbar-toggle collapsed">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="fa fa-th fa-2x"></span>
				      </button>
				    </div>
					<div class="nav_main_list custom-scroll-bar pull-left" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav" id="hover_slip">
							<li><a href="index.php">Home</a></li>
							<li class="arrow_down"><a href="about.html">About Us</a>
								<div class="sub-menu">
									<ul>
										<li><a href="advisor.html">Advisor</a></li>
										<li><a href="single-advisor.html">Single Advisor</a></li>
										<li><a href="career.html">Career</a></li>
										<li><a href="testimonial.html">Testimonaials</a></li>
										<li><a href="partners.html">partners</a></li>
									</ul>
								</div>
							</li>
							<li class="arrow_down"><a href="service.html">Services</a>
								<div class="sub-menu">
									<ul>
										<li><a href="service2.html">Service Two</a></li>
										<li><a href="pricing-page.html">Pricing Page</a></li>
									</ul>
								</div>
							</li>
							<li class="arrow_down" ><a href="#">Pages</a>
								<div class="sub-menu">
									<ul>
										<li><a href="404.html">404 Page</a></li>
										<li><a href="no-search-results.html">No Search Result</a></li>
										<li><a href="search-results.html">Search Results</a></li>
										<li><a href="faq.html">FAQ</a></li>
									</ul>
								</div>
							</li>
							<li class="arrow_down" ><a href="portfolio.html">Projects</a>
								<div class="sub-menu">
									<ul>
										<li><a href="portfolio.html">Portfolio One</a></li>
										<li><a href="portfolio2.html">Portfolio Two</a></li>
										<li><a href="portfolio3.html">Portfolio Three</a></li>
										<li><a href="portfolio4.html">Portfolio Four</a></li>
										<li><a href="portfolio5.html">Portfolio Five</a></li>
									</ul>
								</div>
							</li>
							<li class="arrow_down" ><a href="blog-leftside-bar.html">Blogs</a>
								<div class="sub-menu">
									<ul>
										<li><a href="blog-leftside-bar.html">Blog Left Bar</a></li>
										<li><a href="blog-rightside-bar.html">Blog Right Bar</a></li>
										<li><a href="blog-single-column.html">Blog Single Col</a></li>
										<li><a href="blog-two-column.html">Blog Two Col</a></li>
										<li><a href="blog-details.html">Blog Details</a></li>
									</ul>
								</div>
							</li>
							<li class="arrow_down" ><a href="shop-page.html">Shop</a>
								<div class="sub-menu">
									<ul>
										<li><a href="product-details.html">Product Details</a></li>
										<li><a href="cart-page.html">Cart Page</a></li>
										<li><a href="checkout-page.html">Checkout Page</a></li>
									</ul>
								</div>
							</li>
							<li><a class="contact" href="contact-us.html">Contact Us</a></li>
						</ul>						
					</div>
					<div class="find-advisor pull-right">
						<a href="advisor.html" class="advisor ">Find Advisor</a>
					</div>
				</nav> <!-- End Nav -->
			</div> <!-- End Container -->
		</section>
<!-- ======= /mainmenu-area section ======= -->


<!-- ======= Banner ======= -->
		<section class="p0 container-fluid banner about_banner">
			<div class="about_banner_opacity">
				<div class="container">
					<div class="banner_info_about">
						<h1>Portfolio V4</h1>
						<ul>
							<li><a href="index.php">Home</a></li>
							<li><i class="fa fa-angle-right"></i></li>
							<li>Portfolio V4</li>
						</ul>
						
					</div> <!-- End Banner Info -->
				</div> <!-- End Container -->
			</div> <!-- End Banner_opacity -->
		</section> <!-- End Banner -->
<!-- ======= /Banner ======= -->
<!-- ======= Latest Work ========= -->
		<section class="latest_work">
			<div class="container latest_work_title">
				<h2>Our Latest Work</h2>
				<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi.</p>
			</div>
			<div class="work_gallery">
				<div class="container project_row">
					<div class="row">
						<div class="menu_list"> <!-- Menu -->
							<ul class="p0">
								<li class="filter active" data-filter="all">All</li>
								<li class="filter" data-filter=".insurance">Insurance plans</li>
								<li class="filter" data-filter=".debit">Debit</li>
								<li class="filter" data-filter=".invoice">Invoice</li>
								<li class="filter" data-filter=".report">Annual Report</li>
							</ul>
						</div>
						<div id="mixitup_list" class="portfolio_four">
							<div class="col-lg-4 col-md-4 col-sm-4 work_img_four mix debit report">
								<div style="display:inline-block; padding:0px;" class="mask_holder"><img class="img-responsive" src="images/pro/v4-1.jpg" alt="image" title="Anual Company Growth">
									<div class="gallery_mask_hover">
										<a href="images/pro/v4-1.jpg" class="fancybox"><span>Anual Company Growth</span></a>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 work_img_four mix insurance report ">
								<div style="display:inline-block; padding:0px;" class="mask_holder"><img class="img-responsive" src="images/pro/v4-2.jpg" alt="image" title="Anual Company Growth">
									<div class="gallery_mask_hover">
										<a href="images/pro/v4-2.jpg" class="fancybox"><span>Anual Company Growth</span></a>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 work_img_four mix insurance report invoice">
								<div style="display:inline-block; padding:0px;" class="mask_holder"><img class="img-responsive" src="images/pro/v4-3.jpg" alt="image" title="Anual Company Growth">
									<div class="gallery_mask_hover">
										<a href="images/pro/v4-3.jpg" class="fancybox"><span>Anual Company Growth</span></a>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 work_img_four mix insurance invoice ">
								<div style="display:inline-block; padding:0px;" class="mask_holder"><img class="img-responsive" src="images/pro/v4-4.jpg" alt="image" title="Anual Company Growth">
									<div class="gallery_mask_hover">
										<a href="images/pro/v4-4.jpg" class="fancybox"><span>Anual Company Growth</span></a>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 work_img_four mix debit invoice">
								<div style="display:inline-block; padding:0px;" class="mask_holder"><img class="img-responsive" src="images/pro/v4-5.jpg" alt="image" title="Anual Company Growth">
									<div class="gallery_mask_hover">
										<a href="images/pro/v4-5.jpg" class="fancybox"><span>Anual Company Growth</span></a>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 work_img_four mix insurance  ">
								<div style="display:inline-block; padding:0px;" class="mask_holder"><img class="img-responsive" src="images/pro/v4-6.jpg" alt="image" title="Anual Company Growth">
									<div class="gallery_mask_hover">
										<a href="images/pro/v4-6.jpg" class="fancybox"><span>Anual Company Growth</span></a>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 work_img_four mix debit report ">
								<div style="display:inline-block; padding:0px;" class="mask_holder"><img class="img-responsive" src="images/pro/v4-7.jpg" alt="image" title="Anual Company Growth">
									<div class="gallery_mask_hover">
										<a href="images/pro/v4-7.jpg" class="fancybox"><span>Anual Company Growth</span></a>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 work_img_four mix debit insurance report">
								<div style="display:inline-block; padding:0px;" class="mask_holder"><img class="img-responsive" src="images/pro/v4-8.jpg" alt="image" title="Anual Company Growth">
									<div class="gallery_mask_hover">
										<a href="images/pro/v4-8.jpg" class="fancybox"><span>Anual Company Growth</span></a>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 work_img_four mix debit report invoice">
								<div style="display:inline-block; padding:0px;" class="mask_holder"><img class="img-responsive" src="images/pro/v4-9.jpg" alt="image" title="Anual Company Growth">
									<div class="gallery_mask_hover">
										<a href="images/pro/v4-9.jpg" class="fancybox"><span>Anual Company Growth</span></a>
									</div>
								</div>
							</div>
						</div>
					</div> <!-- End row -->
				</div> <!-- End project_row -->
			</div> <!-- End work_gallery -->
		</section> <!-- End latest_work -->
<!-- ======= /Latest Work ========= -->
<!-- ============ free consultation ================ -->
		<section class="container-fluid consultation">
			<div class="container">
				<p>If you have any querry for related investment  ... We are available</p>
				<a href="">Contact us <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</section> <!-- End consultation -->
<!-- ============ /free consultation ================ -->
		
<!-- ============= Footer ================ -->
		<footer>
			<div class="top_footer container-fluid">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-12 part1">
							<a href=""><img src="images/logo-footer.png" alt="Logo"></a>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ali qua. </p>
							<p><i class="fa fa-phone"></i>&nbsp;&nbsp; 1800 - 254 - 9874</p>
							<p>contact@mefinance.com</p>
							<ul class="p0">
								<li><a href=""><i class="fa fa-facebook"></i></a></li>
								<li><a href=""><i class="fa fa-twitter"></i></a></li>
								<li><a href=""><i class="fa fa-google-plus"></i></a></li>
								<li><a href=""><i class="fa fa-linkedin"></i></a></li>
								<li><a href=""><i class="fa fa-skype"></i></a></li>
							</ul>
						</div>
						<div class="col-lg-2 col-md-3 col-sm-12 part2">
							<h5>Our Services</h5>
							<ul class="p0">
								<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Investment Planning</a></li>
								<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Retairment Planning</a></li>
								<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Mutial Funds</a></li>
								<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Saving & Investments</a></li>
								<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Tax Advisory Service</a></li>
							</ul>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-12 part3">
							<h5>Twitter Feeds</h5>
							<div class="twitter"></div>
						</div>
						<div class="col-lg-3 col-md-2 col-sm-12 part4">
							<h5>Flickr Widget</h5>
							<div class="gallery">
								<img src="images/f-1.jpg" alt="image">
								<img src="images/f-2.jpg" alt="image">
								<img src="images/f-3.jpg" alt="image">
								<img src="images/f-4.jpg" alt="image">
								<img src="images/f-5.jpg" alt="image">
								<img src="images/f-6.jpg" alt="image">
							</div>
						</div>
					</div> <!-- End row -->
				</div>
			</div> <!-- End top_footer -->
			<div class="bottom_footer container-fluid">
				<div class="container">
					<p class="float_left">Copyright &copy; MeFinance 2015. All rights reserved. </p>
					<p class="float_right">Created by: DesignArc</p>
				</div>
			</div> <!-- End bottom_footer -->
		</footer>
<!-- ============= /Footer =============== -->

		<!-- Js File -->

		<!-- j Query -->
				<script type="text/javascript" src="js/jquery-2.1.4.js"></script>
		<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
		<script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
		<script src="js/revolution-slider/jquery.themepunch.tools.min.js"></script> <!-- Revolution Slider Tools -->
		<script src="js/revolution-slider/jquery.themepunch.revolution.min.js"></script> <!-- Revolution Slider -->
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.actions.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.carousel.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.kenburn.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.layeranimation.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.migration.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.navigation.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.parallax.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.slideanims.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.video.min.js"></script>

		<!-- Bootstrap JS -->
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/jquery.appear.js"></script>
		<script type="text/javascript" src="js/jquery.countTo.js"></script>
		<script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
		<!-- Custom & Vendor js -->
		<script type="text/javascript" src="js/custom.js"></script>
		<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
		<script src="js/jquery.sliphover.min.js"></script>

		

	</body>
</html>