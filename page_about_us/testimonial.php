



<!-- ======= Banner ======= -->

		<section class="p0 container-fluid banner about_banner">

			<div class="about_banner_opacity">

				<div class="container">

					<div class="banner_info_about">

						<h1>Testimoni</h1>

						<ul>

							<li>Tentang Kami</li>

							<li><i class="fa fa-angle-right"></i></li>

							<li>Testimoni</li>

						</ul>

						

					</div> <!-- End Banner Info -->

				</div> <!-- End Container -->

			</div> <!-- End Banner_opacity -->

		</section> <!-- End Banner -->

<!-- ======= /Banner ======= -->

<!-- ============ what our client say =============== -->

		<section class="client_say_about_us">

			<div class="container">

				<div class="row">

					<div class="col-lg-6 col-md-6 col-sm-12 testimonial left_part">

						<img class="round_img" src="images/testimonial/ceo_1.jpg" alt="images">

						<div class="float_right client_info">

							<p>Jefri Sumardiono</p>

							<span>Chief Executive Officer</span>

							<ul>

								<li><a href=""></a></li>

								<li><a href=""></a></li>

								<li><a href=""></a></li>

								<li><a href=""></a></li>

								<li><a href=""></a></li>

							</ul>

						</div>

						<p class="john_speach clear_fix">Pekerjaan sekecil apa pun akan membesarkan kehidupanmu, jika kamu melakukannya dengan kesungguhan hati. </p>

						<!--<img class="client_sign" src="images/testimonial/sign.jpg" alt="images">-->

					</div>

					<div class="col-lg-6 col-md-6 col-sm-12 testimonial right_part">

						<img class="round_img" src="images/testimonial/ceo_2.jpg" alt="images">

						<div class="float_right client_info">

							<p>Ujang Fajar Awaluddin</p>

							<span>Chief Operating Officer</span>

							<ul>

								<li><a href=""></a></li>

								<li><a href=""></a></li>

								<li><a href=""></a></li>

								<li><a href=""></a></li>

								<li><a href=""></a></li>

							</ul>

						</div>

						<p class="john_speach clear_fix">Inginkanlah sesuatu berdasar kadar kemampuan yang kau miliki jangan menginginkan sesuatu berdasar kadar nafsu dan seleramu. </p>

						<!--<img class="client_sign" src="images/testimonial/sign.jpg" alt="images">-->

					</div>

					<div class="col-lg-6 col-md-6 col-sm-12 testimonial left_part bottom_part">

						<img class="round_img" src="images/testimonial/ceo_3.jpg" alt="images">

						<div class="float_right client_info">

							<p>Anang Shaleh Bakti</p>

							<span>Chief Technology Officer</span>

							<ul>

								<li><a href=""></a></li>

								<li><a href=""></a></li>

								<li><a href=""></a></li>

								<li><a href=""></a></li>

								<li><a href=""></a></li>

							</ul>

						</div>

						<p class="john_speach clear_fix">Stay Foolish, Stay Hungry (Steve Jobs). </p>

						<!--<img class="client_sign" src="images/testimonial/sign.jpg" alt="images">-->

					</div>

					<!--<div class="col-lg-6 col-md-6 col-sm-12 testimonial right_part bottom_part">

						<img class="round_img" src="images/testimonial/4.jpg" alt="images">

						<div class="float_right client_info">

							<p>MeFinance is Great Firm</p>

							<span>Michale John (CEO & Founder)</span>

							<ul>

								<li><a href=""></a></li>

								<li><a href=""></a></li>

								<li><a href=""></a></li>

								<li><a href=""></a></li>

								<li><a href=""></a></li>

							</ul>

						</div>

						<p class="john_speach clear_fix">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipis civelit sed quia non qui dolorem ipsum quia dolor sit amet, consectetur, adipis civelit sed quia numquam eius modi. Neque porro quisquam est, qui dolorem ipsum </p>

						<img class="client_sign" src="images/testimonial/sign.jpg" alt="images">

					</div>-->

				</div>

			</div>

		</section>

<!-- ============ /what our client say =============== -->

<!-- ============= Client slider ==================== -->

		<!--<section class="client_slider">

			<div class="section_opacity">

				<div class="container">

					<div class="row">

						<div class="col-lg-2 client_img">

							<img class="img-responsive" src="images/testimonial/5.jpg" alt="images">

							<ul class="p0">

								<li><a href=""></a></li>

								<li><a href=""></a></li>

								<li><a href=""></a></li>

								<li><a href=""></a></li>

								<li><a href=""></a></li>

							</ul>

						</div>

						<div class="col-lg-8 client_speach">

							<p><i class="fa fa-quote-left"></i>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipis civelit sed quia non qui dolorem ipsum quia dolor sit amet, consectetur, adipis civelit sed quia numquam eius modi. Neque porro quisquam est, qui dolorem ipsum </p>

							<span>- Michale John (CEO & Founder)</span>

						</div>

					</div>

				</div>

			</div>

		</section>-->

<!-- ============= Client slider ==================== -->



		

