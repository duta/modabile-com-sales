
<!-- ======= Banner ======= -->
		<section class="p0 container-fluid banner about_banner">
			<div class="about_banner_opacity">
				<div class="container">
					<div class="banner_info_about">
						<h1>Maintenance System</h1>
						<ul>
							<li><a href="index.php">Home</a></li>
							<li><i class="fa fa-angle-right"></i></li>
							<li>Maintenance System</li>
						</ul>
						
					</div> <!-- End Banner Info -->
				</div> <!-- End Container -->
			</div> <!-- End Banner_opacity -->
		</section> <!-- End Banner -->
<!-- ======= /Banner ======= -->
		<section class="side_tab">
			<div class="container">
				<div class="row">
					<div class="white_bg right_side col-lg-8 col-md-8 col-sm-12 col-xs-12">
						<div>
							<div class="float_left">
								<p>Bagian dari layanan kami adalah memelihara dan memastikan System yang kita kembangkan dengan user berjalan dengan baik setiap saat, sehingga jika klien menghendaki kami juga melakukan perawatan Software yang kami kembangkan dan kami sudah memiliki team yang solid.</p>
							</div>
						</div>
					</div> <!-- End white_bg -->
				</div> <!-- End row -->
			</div> <!-- End container -->
		</section>
