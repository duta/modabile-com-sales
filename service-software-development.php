
<!-- ======= Banner ======= -->
		<section class="p0 container-fluid banner about_banner">
			<div class="about_banner_opacity">
				<div class="container">
					<div class="banner_info_about">
						<h1>Software Development</h1>
						<ul>
							<li><a href="index.php">Home</a></li>
							<li><i class="fa fa-angle-right"></i></li>
							<li>Software Development</li>
						</ul>
						
					</div> <!-- End Banner Info -->
				</div> <!-- End Container -->
			</div> <!-- End Banner_opacity -->
		</section> <!-- End Banner -->
<!-- ======= /Banner ======= -->
		<section class="side_tab">
			<div class="container">
				<div class="row">
					<div class="white_bg right_side col-lg-8 col-md-8 col-sm-12 col-xs-12">
						
						<div>
							<div class="float_left">
								<p>Saat ini kami memiliki team Engineer yang terus menerus secara kontinyu mengembangkan Aplikasi Vessel Tracking System. Sehingga Kami siap melayani order dari Anda jika ada kebutuhan khusus Aplikasi Vessel Tracking System. <br> <br>
Karena kami mengembangkan System ini secara mandiri kami juga menerima jasa modifikasi Aplikasi Tracking System sesuai dengan pesanan pelanggan. Misalkan untuk dikembangkan bisa memantau posisi armada baik berbasis AIS Receiver, AIS Satelit, Wifi GPS dan lain sebagainya.
</p>
							</div>
						</div>
					</div> <!-- End white_bg -->
				</div> <!-- End row -->
			</div> <!-- End container -->
		</section>

