<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>

<style type="text/css">
.error
{
color:red;
font-family:verdana, Helvetica;
}
</style>
</head>

<body>
<!--<form id="myform" action="#">
  
 <p>
   <label for="fullname"> Name:</label><input type="text" name="fullname" /></p><p>
  <label for="email"> Email:</label><input type="text" name="email" />   
  </p>
  <h3>Items </h3>
  <table border="0"  id="items_table">
  <tr>
  <td>Item</td>
  <td>Quantity</td>
  <td> </td>
  </tr>
    <tr id="master_tr">
    <td>
    <select name="item" class="itemsel" size="1">
      <option value="">Select ...</option>
        <option value="Avocado">Avocado</option>
        <option value="Bell Pepper">Bell Pepper</option> 
      <option value="Bitter Melon">Bitter Melon</option> 
      <option value="Chayote">Chayote</option>
        <option value="Cucumber">Cucumber</option>

      </select>
  </td>
  <td><input type="text" name="qty" class="itemqty" ></td>
  <td><a href="javascript:void(0);" id="addbutton">Add</a> | <a href="javascript:void(0);" id="removebutton">Remove</a></td>
  </tr>
  </table>
  <input type="submit" name="submit" value="Submit">
</form>-->



<div class="container">
<div class="get_touch">
 <form class="form-inline" role="form">
 <fieldset><legend>Request Kapal</legend></fieldset>
  <div class="form-group">
    <input type="text" class="form-control" id="mmsi" placeholder="MMSI">
    <input type="text" class="form-control" id="namaKapal" placeholder="Nama Kapal">      
    <button type="submit" class="info-inline"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>      
 </div>
  <br>
  
  <div class="form-group">
    <input type="text" class="form-control" id="mmsi" placeholder="MMSI">
    <input type="text" class="form-control" id="namaKapal" placeholder="Nama Kapal">      
    <button type="submit" class="info-inline"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>  
    <button type="submit" class="danger-inline"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
 </div>
  <br>  

  <div class="form-group">
    <input type="text" class="form-control" id="mmsi" placeholder="MMSI">
    <input type="text" class="form-control" id="namaKapal" placeholder="Nama Kapal">      
    <button type="submit" class="info-inline"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>  
    <button type="submit" class="danger-inline"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
 </div>
  <br>
  
<div class="form">   
<button type="submit" class="submit" name="simpan" id="submit" onClick="submitForm('formA','ajax.crud.h.php')">Request<i class="fa fa-arrow-circle-right"></i></button>  
</div>
</form>
</div>
</div>
<?php
include('_koneksi.php');
// prepare and bind
if(isset($_POST['mmsi'])){
	$sql = $con->prepare("INSERT INTO detailkapal (idUser, mmsi, namaKapal) VALUES (?, ?, ?)");
	$sql->bind_param("sss", $idUser, $mmsi, $namaKapal);
	
	// set parameters and execute
	$idUser = 1;
	$mmsi = 12;
	$namaKapal = "Fajar Abadi";
	$sql->execute();
	
	echo "New records created successfully";
	
	$sql->close();
	$con->close();
}
?> 

</body>
</html>
<script>

$(function()
  {
    var n_item=1;
    $('#addbutton').click(function()
    {
      var $trcopy = $('#items_table tr#master_tr').clone(true);
      $trcopy.attr('id','');
      n_item++;
      $('.itemsel',$trcopy).attr('name','item'+n_item);
      $('.itemqty',$trcopy).attr('name','qty'+n_item);      
      $('#removebutton',$trcopy).show();
      
      $('#items_table').append($trcopy);
      //Need to add the element to the form first (see the append above)
      //Then call the rules() function
      $('.itemsel',$trcopy).rules('add',
                {
                  required:true,
                  messages:
                  {
                     required:"Please select item"
                   }
                 });
      
      $('.itemqty',$trcopy).rules('add', 
                 {
                    required:true,
                    min:1,
                    max:5,
                    messages:
                    {
                      required:"Please enter quantity",
                      min:"minimum quantity {0}",
                      max:"max quantity {0}"
                    }
                  });
      
    });
    $('#removebutton').click(function()
    {
      var $tr = $(this).parents('tr');  
      $tr.remove();
    })
    
    $('#items_table tr#master_tr #removebutton').hide();
    
    $('#myform').validate(
      { 
      rules:
        { 
          fullname:{ required:true },
          email:{required:true},
          'qty':{required:true}
        },
        errorPlacement: function(error, element) 
        {
            if ( element.parents("#items_table").length > 0 ) 
            {
              var $errbox = element.parents('tr').next('tr.error');
              var errortxt = error.text();
              if($errbox.length > 0)
              {
                if(errortxt==='')
                {
                  $errbox.remove();
                }
                else
                {
                  $('td',$errbox).text(errortxt);
                }
              }
              else
              {
                element.parents('tr').after("<tr class='error'><td colspan='3'>"+error.text()+"</td></tr>");
              }
            }
            else 
            { 
                error.insertAfter( element );
            }
        }, 
        success: function (label, element) 
        {
          
          if ( $(element).parents("#items_table").length > 0 ) 
          {
            var $errbox = $(element).parents('tr').next('tr.error');
            $errbox.remove();
            $(element).removeClass('error');
          }
        }
      });
    
  });
</script>
