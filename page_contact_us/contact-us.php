	<div class="contact_page">

<!-- ======= Banner ======= -->
		<section class="p0 container-fluid banner about_banner">
			<div class="about_banner_opacity">
				<div class="container">
					<div class="banner_info_about">
						<h1>Cara Order</h1>
						<!--<ul>
							<li><a href="index.php">Home</a></li>
							<li><i class="fa fa-angle-right"></i></li>
							<li>Cara Order</li>
						</ul>-->
					</div> <!-- End Banner Info -->
				</div> <!-- End Container -->
			</div> <!-- End Banner_opacity -->
		</section> <!-- End Banner -->
<!-- ================= /Banner ================ -->
<!-- =================== Contact us container ============== -->
		<section class="contact_us_container">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align:center;"> <!-- section title -->
						<h2>Hubungi Kami</h2>
						<p>Apabila perusahaan anda menghendaki presentasi dan demo perangkat harware dan software Silahkan menghubungi  Produk representatif kami 
</p>
					</div> <!-- End section title -->
					<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 form_holder"> <!-- form holder -->
						<form action="send-email.php" method="POST" class="contact-form">
							<div class="alert alert-success" style="display:none;">
	                        </div>
							<input class="form-control name" type="text" name="name" placeholder="Your Name">
							<input class="form-control email" type="email" name="email" placeholder="Your Email">
						    <input class="form-control" type="text" name="subject" placeholder="Subject">
						    <textarea class="form-control" name="message" placeholder="Message"></textarea><br>
						    <div class="g-recaptcha" data-sitekey="6LdU5iwUAAAAAPUMNN5gDtpZu1LDOVqL4l5khOmV" data-type="image" data-theme="light"></div>
						    <button type="submit" class="submit">submit now <i class="fa fa-arrow-circle-right"></i></button>
						</form> <!-- End form holder -->
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pull-right address">
						<address>
							<div class="icon_holder float_left"><span class="icon icon-Pointer"></span></div>
							<div class="address_holder float_left">Jl. Kebraon Village BM-20
  <br> Karang Pilang - Surabaya <br>Indonesia</div>
						</address>
						<address class="clear_fix">
							<div class="icon_holder float_left"><span class="icon icon-Plaine"></span></div>
							<div class="address_holder float_left">marketing@ptdmc.co.id <br>
							</div>
						</address>
						<address class="clear_fix">
							<div class="icon_holder float_left"><span class="icon icon-Phone2"></span></div>
							<div class="address_holder float_left">+ (62) 31 766 9882 <br> + (62) 81230900090<br></div>
						</address>
					</div>
				</div>
			</div>
		</section>

<!-- =================== /Contact us container ============== -->

<!-- =============== google map ============= -->
		<div class="map">
			<div class="google-map" id="contact-google-map" data-map-lat="-7.327000" data-map-lng="112.697250" data-icon-path="images/map-marker.png" data-map-title="Awesome Place" data-map-zoom="12"></div>
		</div>
<!-- =============== /google map ============= -->



	</div>