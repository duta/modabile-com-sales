<section class="welcome_sec">
			<div class="container">
				<div class="row welcome_heading">
					<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
						<h2>Kami Menawarkan <br>Layanan Yang Berbeda</h2>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
						<p></p>
					</div>
				</div> <!-- End Row -->
<?php //include("slide3.php");?>
				<div class="row welcome welcome_details">
					<div class="col-lg-6 col-md-12">
						<div class="welcome_item">
							<img src="images/product/radar-furuno-123-S.jpg" alt="AIS">
							<div class="welcome_info">
								<h3>Radar</h3>
								<p>Team Modabile juga melayani kebutuhan pengadaan dan instalasi Radar Dari beberapa merk yang team kuasai.</p>
							</div>
						</div>
						<div class="welcome_item welcome_item_bottom">
							<img src="images/product/AIS-S.jpg" alt="Radar">
							<div class="welcome_info">
								<h3>AIS</h3>
								<p>Team Modabile melayani kebutuhan perangkat AIS baik itu AIS Receiver, AIS Base Station Maupun AIS Kapal perusahaan anda.</p>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-12 bottom_row">
						<div class="welcome_item">
							<img src="images/product/Radio-telekomunikasi-fm_4721-S.jpg" alt="Radio Telekomunikasi" >
							<div class="welcome_info">
								<h3>Radio Telekomunikasi</h3>
								<p>Team Modabile melayani kebutuhan perangkat radio komunikasi perusahaan anda baik teknologi radio analog maupun digital</p>
							</div>
						</div>
						<div class="welcome_item welcome_item_bottom">
							<img src="images/product/portable pilot point.jpg" alt="Portable Pilot Unit">
                            <!--<img src="images/4.jpg" alt="images">-->
							<div class="welcome_info">
								<h3>Portable Pilot Unit</h3>
								<p>Team Modabile juga melayani kebutuhan perangkat dan sistem navigasi petugas pandu yang cukup Modern yakni Portable Pilot Unit dari produk TRANSAS</p>
							</div>
						</div>
					</div>
				</div> <!-- End Row -->                
			</div> <!-- End container -->
		</section>
        
<!--<script>
(function() {  
    var dialog = document.getElementById('window');  
    document.getElementById('show').onclick = function() {  
        dialog.show();  
    };  
    document.getElementById('exit').onclick = function() {  
        dialog.close();  
    };  
})();
</script>-->