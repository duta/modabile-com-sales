	<div class="service">

<!-- ======= Banner ======= -->
		<section class="p0 container-fluid banner about_banner">
			<div class="about_banner_opacity">
				<div class="container">
					<div class="banner_info_about">
						<h1>Integrasi Sistem</h1>
						<ul>
							<li><a href="index.php">Home</a></li>
							<li><i class="fa fa-angle-right"></i></li>
							<li>Services</li>
                            <li><i class="fa fa-angle-right"></i></li>
                            <li>Integrasi Sistem</li>
						</ul>
						
					</div> <!-- End Banner Info -->
				</div> <!-- End Container -->
			</div> <!-- End Banner_opacity -->
		</section> <!-- End Banner -->
<!-- ======= /Banner ======= -->
<!-- ======== Service facts ========= -->
		<section class="service_facts">
			<div class="container">
				<div class="row">
					<div class="white_bg right_side col-lg-8 col-md-8 col-sm-12 col-xs-12">
						
						<div>
							<div class="float_left">
								<p>Tidak menutup kemungkinan Pelanggan menghendaki Aplikasi Vessel Tracking System yang dibangun dan kembangkan harus terintegrasi dengan System perusahaan yang lain. Sehingga dibutuhkan support layanan Jasa Integresi dengan System perusahaan yang lain.
</p>
							</div>
						</div>
					</div> <!-- End white_bg -->
				</div> <!-- End row -->
			</div>
		</section>
<!-- ======== /Service facts ========= -->
	</div>