<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">

		<!-- For IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- For Resposive Device -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>Me-Financial</title>

		<!-- Favicon -->
		<link rel="apple-touch-icon" sizes="57x57" href="images/fav-icon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="images/fav-icon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="images/fav-icon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="images/fav-icon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="images/fav-icon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="images/fav-icon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="images/fav-icon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="images/fav-icon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="images/fav-icon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="images/fav-icon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="images/fav-icon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="images/fav-icon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="images/fav-icon/favicon-16x16.png">


		<!-- Bootstrap CSS -->
		<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.css" media="screen">


		<!-- Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500italic,500,700,700italic' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,300,600,700,800,900' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,700,700italic,800,300,300italic' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=PT+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

		<!-- Font Awesome -->
		<link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css">
		<!-- Stroke Gap Icon -->
		<link rel="stylesheet" href="fonts/stroke-gap/style.css">

		<!-- Custom Css -->
		<link rel="stylesheet" type="text/css" href="css/custom/style.css">
		<link rel="stylesheet" type="text/css" href="css/responsive/responsive.css">


		<!--[if lt IE 9]>
	   		<script src="js/html5shiv.js"></script>
		<![endif]-->



	</head>
	<body class="advisor_single">

<!-- =======Header ======= -->
		<header>
			<div class="container-fluid top_header">
				<div class="container">
					<p class="float_left">Welcome to Me Financial Services, we have over 12 years of expertise</p>
					<div class="float_right">
						<ul>
							<li><a href=""><i class="fa fa-facebook"></i></a></li>
							<li><a href=""><i class="fa fa-twitter"></i></a></li>
							<li><a href=""><i class="fa fa-google-plus"></i></a></li>
							<li><a href=""><i class="fa fa-linkedin"></i></a></li>
							<li>
								<div  id="search_box">
									<input id="search" type="text" placeholder="Search here">
									<button id="button" type="submit"><span class="icon icon-Search"></span></button>
								</div>
							</li>
						</ul>
					</div>
				</div> <!-- end container -->
			</div><!-- end top_header -->
			<div class="bottom_header top-bar-gradient">
				<div class="container clear_fix">
					<div class="float_left logo">
						<a href="index.php">
							<img src="images/logo.png" alt="Me Finance">
						</a>
					</div>
					<div class="float_right address">
						<div class="top-info">
							<div class="icon-box">
								<span class=" icon icon-Pointer"></span>							
							</div>
							<div class="content-box">
								<p> 13005 Greenville Avenue <br><span>California, TX 70240</span></p>
							</div>
						</div>
						<div class="top-info">
							<div class="icon-box">
								<span class="separator icon icon-Phone2"></span>							
							</div>
							<div class="content-box">
								<p>+ (1800) 456 7890 <br><span>info@mefinance.com</span></p>
							</div>
						</div>
						<div class="top-info">
							<div class="icon-box">
								<span class="separator icon icon-Timer"></span>
							</div>
							<div class="content-box">
								<p>Mon - Sat 9.00 - 19.00 <br><span>Sunday Closed</span></p>
							</div>
						</div>
					</div>
				</div> <!-- end container -->
			</div> <!-- end bottom_header -->
		</header> <!-- end header -->
<!-- ======= /Header ======= -->

<!-- ======= mainmenu-area section ======= -->
		<section class="mainmenu-area stricky">
			<div class="container">
				<nav class="clearfix">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header clearfix">
				      <button type="button" class="navbar-toggle collapsed">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="fa fa-th fa-2x"></span>
				      </button>
				    </div>
					<div class="nav_main_list custom-scroll-bar pull-left" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav" id="hover_slip">
							<li><a href="index.php">Home</a></li>
							<li class="arrow_down"><a href="about.html">About Us</a>
								<div class="sub-menu">
									<ul>
										<li><a href="advisor.html">Advisor</a></li>
										<li><a href="single-advisor.html">Single Advisor</a></li>
										<li><a href="career.html">Career</a></li>
										<li><a href="testimonial.html">Testimonaials</a></li>
										<li><a href="partners.html">partners</a></li>
									</ul>
								</div>
							</li>
							<li class="arrow_down"><a href="service.html">Services</a>
								<div class="sub-menu">
									<ul>
										<li><a href="service2.html">Service Two</a></li>
										<li><a href="pricing-page.html">Pricing Page</a></li>
									</ul>
								</div>
							</li>
							<li class="arrow_down" ><a href="#">Pages</a>
								<div class="sub-menu">
									<ul>
										<li><a href="404.html">404 Page</a></li>
										<li><a href="no-search-results.html">No Search Result</a></li>
										<li><a href="search-results.html">Search Results</a></li>
										<li><a href="faq.html">FAQ</a></li>
									</ul>
								</div>
							</li>
							<li class="arrow_down" ><a href="portfolio.html">Projects</a>
								<div class="sub-menu">
									<ul>
										<li><a href="portfolio.html">Portfolio One</a></li>
										<li><a href="portfolio2.html">Portfolio Two</a></li>
										<li><a href="portfolio3.html">Portfolio Three</a></li>
										<li><a href="portfolio4.html">Portfolio Four</a></li>
										<li><a href="portfolio5.html">Portfolio Five</a></li>
									</ul>
								</div>
							</li>
							<li class="arrow_down" ><a href="blog-leftside-bar.html">Blogs</a>
								<div class="sub-menu">
									<ul>
										<li><a href="blog-leftside-bar.html">Blog Left Bar</a></li>
										<li><a href="blog-rightside-bar.html">Blog Right Bar</a></li>
										<li><a href="blog-single-column.html">Blog Single Col</a></li>
										<li><a href="blog-two-column.html">Blog Two Col</a></li>
										<li><a href="blog-details.html">Blog Details</a></li>
									</ul>
								</div>
							</li>
							<li class="arrow_down" ><a href="shop-page.html">Shop</a>
								<div class="sub-menu">
									<ul>
										<li><a href="product-details.html">Product Details</a></li>
										<li><a href="cart-page.html">Cart Page</a></li>
										<li><a href="checkout-page.html">Checkout Page</a></li>
									</ul>
								</div>
							</li>
							<li><a class="contact" href="contact-us.html">Contact Us</a></li>
						</ul>						
					</div>
					<div class="find-advisor pull-right">
						<a href="advisor.html" class="advisor ">Find Advisor</a>
					</div>
				</nav> <!-- End Nav -->
			</div> <!-- End Container -->
		</section>
<!-- ======= /mainmenu-area section ======= -->


<!-- ======= Banner ======= -->
		<section class="p0 container-fluid banner about_banner">
			<div class="about_banner_opacity">
				<div class="container">
					<div class="banner_info_about">
						<h1>Single Advisor</h1>
						<ul>
							<li><a href="index.php">Home</a></li>
							<li><i class="fa fa-angle-right"></i></li>
							<li>Single Advisor</li>
						</ul>
						
					</div> <!-- End Banner Info -->
				</div> <!-- End Container -->
			</div> <!-- End Banner_opacity -->
		</section> <!-- End Banner -->
<!-- =============== /Banner =============== -->

<!-- =============== Michale John =============== -->
		<section class="advisor_name container">
			<div class="row">
				<div class="col-lg-4 col-md-4 advisor_img">
					<img class="img-responsive" src="images/17.jpg" alt="image">
					<img class="img-responsive" src="images/shadow.png" alt="image">
				</div>
				<div class="col-lg-8 col-md-8 advisor_contact_info">
					<h4>Michale John</h4>
					<p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit.</p>
					<ul>
						<li><i class="fa fa-phone"></i> + 91 865 9575 963</li>
						<li><i class="fa fa-envelope-o"></i><a href="mailto:john@mefinance.com">john@mefinance.com</a></li>
					</ul>
					<h4>Specialization :</h4>
					<ul>
						<li><a href=""><i class="fa fa-angle-right"></i>Taxes and Accounting</a></li>
						<li><a href=""><i class="fa fa-angle-right"></i>Analysis of profitability</a></li>
						<li><a href=""><i class="fa fa-angle-right"></i>Profit model and much more</a></li>
					</ul>
				</div>
			</div>
		</section>
<!-- =============== /Michale John =============== -->
<!-- =============== Professional Experiences =============== -->
		<section class="experiences_sec">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<h2>Professional Experiences</h2>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
						<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? <br> <br>
						<span>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,</span>
						sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. </p>
					</div>
				</div>
			</div>
		</section> <!-- End experiences_sec -->
<!-- =============== /Professional Experiences =============== -->

<!-- =============== Education And Tranining =============== -->
		<section class="education_sec">
			<div class="container">
				<div class="col-lg-4 col-md-4 col-sm-12 education_title_holder" style="padding-left: 0px;">
					<h2>Education And Tranining</h2>
				</div>
				<div class="col-lg-1 col-md-1 col-sm-2 col-xs-2 education_years_holder">
					<span>1993</span>
					<span>1997</span>
					<span>2002</span>
					<span>2005</span>
				</div>
				<div class="col-lg-7 col-md-7 col-sm-10 col-xs-10">
					<div class="education_category">
						<h3>completed BA in Accounts</h3>
						<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni </p>
					</div>
					<div class="education_category">
						<h3>completed MA in Accounts and economics</h3>
						<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni </p>
					</div>
					<div class="education_category">
						<h3>Completed PHD in accounts and economics</h3>
						<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni </p>
					</div>
					<div class="education_category">
						<h3>Get Training from Jingal accounting firm</h3>
						<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni </p>
					</div>
				</div>
			</div>
		</section>
<!-- =============== /Education And Tranining =============== -->
<!-- =============== My Skills  =============== -->
		<section class="my_skills_sec">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 skills_title_holder">
						<h2>My Skills</h2>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 progress_item">
						<h4>financial analysis</h4>
						<div class="progress" data-value="62">
						  <div class="progress-bar" role="progressbar" aria-valuenow="62" aria-valuemin="0" aria-valuemax="100">
						    <div class="value-holder"><span class="value"></span>%</div>
						  </div>
						</div>
						<h4>Profitable investment</h4>
						<div class="progress" data-value="90">
						  <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
						    <div class="value-holder"><span class="value"></span>%</div>
						  </div>
						</div>
						<h4>business support</h4>
						<div class="progress" data-value="75" >
						  <div class="progress-bar" role="progressbar"  aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">
						    <div class="value-holder"><span class="value"></span>%</div>
						  </div>
						</div>
						<h4>Taxes and Accounting</h4>
						<div class="progress" data-value="50" >
						  <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
						    <div class="value-holder"><span class="value"></span>%</div>
						  </div>
						</div>
					</div>
				</div>
			</div>
		</section>
<!-- =============== /My Skills  =============== -->

<!-- =============== live comment  =============== -->
		<section class="leave_comment_sec">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 comment_title_holder">
						<h2>Leave A Comment</h2>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"> <!-- comment form -->
						<form action="#" method="get" id="leave_comment">
							<input class="form-control name" type="text" name="yourname" placeholder="First Name">
							<input class="form-control email" type="email" name="yourmail" placeholder="Email Address">
						    <input class="form-control" type="text" name="yournumber" placeholder="Phone No.">
						    <textarea class="form-control" name="" id="" cols="30" rows="10" placeholder="Message"></textarea>
						    <button type="submit" class="submit">submit now <i class="fa fa-arrow-circle-right"></i></button>
						</form> <!-- End Comment form -->
					</div>
				</div>
			</div>
		</section>
<!-- =============== /live comment  =============== -->

<!-- ============ free consultation ================ -->
		<section class="container-fluid consultation">
			<div class="container">
				<p>If you have any querry for related investment  ... We are available</p>
				<a href="">Contact us <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</section> <!-- End consultation -->
<!-- ============ /free consultation ================ -->

<!-- ============= Footer ================ -->
		<footer>
			<div class="top_footer container-fluid">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-12 part1">
							<a href=""><img src="images/logo-footer.png" alt="Logo"></a>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ali qua. </p>
							<p><i class="fa fa-phone"></i>&nbsp;&nbsp; 1800 - 254 - 9874</p>
							<p>contact@mefinance.com</p>
							<ul class="p0">
								<li><a href=""><i class="fa fa-facebook"></i></a></li>
								<li><a href=""><i class="fa fa-twitter"></i></a></li>
								<li><a href=""><i class="fa fa-google-plus"></i></a></li>
								<li><a href=""><i class="fa fa-linkedin"></i></a></li>
								<li><a href=""><i class="fa fa-skype"></i></a></li>
							</ul>
						</div>
						<div class="col-lg-2 col-md-3 col-sm-12 part2">
							<h5>Our Services</h5>
							<ul class="p0">
								<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Investment Planning</a></li>
								<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Retairment Planning</a></li>
								<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Mutial Funds</a></li>
								<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Saving & Investments</a></li>
								<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Tax Advisory Service</a></li>
							</ul>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-12 part3">
							<h5>Twitter Feeds</h5>
							<div class="twitter"></div>
						</div>
						<div class="col-lg-3 col-md-2 col-sm-12 part4">
							<h5>Flickr Widget</h5>
							<div class="gallery">
								<img src="images/f-1.jpg" alt="image">
								<img src="images/f-2.jpg" alt="image">
								<img src="images/f-3.jpg" alt="image">
								<img src="images/f-4.jpg" alt="image">
								<img src="images/f-5.jpg" alt="image">
								<img src="images/f-6.jpg" alt="image">
							</div>
						</div>
					</div> <!-- End row -->
				</div>
			</div> <!-- End top_footer -->
			<div class="bottom_footer container-fluid">
				<div class="container">
					<p class="float_left">Copyright &copy; MeFinance 2015. All rights reserved. </p>
					<p class="float_right">Created by: DesignArc</p>
				</div>
			</div> <!-- End bottom_footer -->
		</footer>
<!-- ============= /Footer =============== -->

		<!-- Js File -->

		<!-- j Query -->
				<script type="text/javascript" src="js/jquery-2.1.4.js"></script>
		<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
		<script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
		<script src="js/revolution-slider/jquery.themepunch.tools.min.js"></script> <!-- Revolution Slider Tools -->
		<script src="js/revolution-slider/jquery.themepunch.revolution.min.js"></script> <!-- Revolution Slider -->
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.actions.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.carousel.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.kenburn.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.layeranimation.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.migration.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.navigation.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.parallax.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.slideanims.min.js"></script>
		<script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.video.min.js"></script>

		<!-- Bootstrap JS -->
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/jquery.appear.js"></script>
		<script type="text/javascript" src="js/jquery.countTo.js"></script>
		<script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
		<!-- Custom & Vendor js -->
		<script type="text/javascript" src="js/custom.js"></script>
		<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>

		

	</body>
</html>