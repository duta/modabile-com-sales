
	<div class="shop">

<!-- ======= Banner ======= -->
		<section class="p0 container-fluid banner about_banner">
			<div class="about_banner_opacity">
				<div class="container">
					<div class="banner_info_about">
						<h1>Radar</h1>
						<ul>
							<li><a href="index.php">Home</a></li>
							<li><i class="fa fa-angle-right"></i></li>
							<li><a href="?r=product-all.php">Product</a></li>
                            <li><i class="fa fa-angle-right"></i></li>
                            <li>Radar</li>
						</ul>
						
					</div> <!-- End Banner Info -->
				</div> <!-- End Container -->
			</div> <!-- End Banner_opacity -->
		</section> <!-- End Banner -->
<!-- ======= /Banner ======= -->
<!-- ==================shop ================ -->
		<section class="shop_container faqs_sec"> <!-- faqs_sec use for style left side content -->
			<div class="container">
				<div class="row">
					<!-- .shop-page-content -->
					<div class="col-lg-8 col-md-8 pull-right shop-page-content">
						<div class="section-title-style-2">
							<h2>Radar</h2>
						</div>				
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 single-shop-item">
								<img class="img-responsive" src="images/product/radar-244-180.jpg" alt="">
								<div class="meta">
									<h4>Radar</h4>
									<p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit,</p>
									<span>Price: <b>$24.5</b></span>
									<a href="?r=product-chart">Add to Cart <i class="fa fa-shopping-cart"></i></a>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 single-shop-item">
								<img class="img-responsive" src="images/product/radar-244-180.jpg" alt="">
								<div class="meta">
									<h4>Radar</h4>
									<p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit,</p>
									<span>Price: <b>$24.5</b></span>
									<a href="?r=product-chart">Add to Cart <i class="fa fa-shopping-cart"></i></a>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 single-shop-item">
								<img class="img-responsive" src="images/product/radar-244-180.jpg" alt="">
								<div class="meta">
									<h4>Radar</h4>
									<p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit,</p>
									<span>Price: <b>$24.5</b></span>
									<a href="?r=product-chart">Add to Cart <i class="fa fa-shopping-cart"></i></a>
								</div>
							</div>
						</div>		
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 single-shop-item">
								<img class="img-responsive" src="images/product/radar-244-180.jpg" alt="">
								<div class="meta">
									<h4>Radar</h4>
									<p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit,</p>
									<span>Price: <b>$24.5</b></span>
									<a href="?r=product-chart">Add to Cart <i class="fa fa-shopping-cart"></i></a>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 single-shop-item">
								<img class="img-responsive" src="images/product/radar-244-180.jpg" alt="">
								<div class="meta">
									<h4>Radar</h4>
									<p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit,</p>
									<span>Price: <b>$24.5</b></span>
									<a href="?r=product-chart">Add to Cart <i class="fa fa-shopping-cart"></i></a>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 single-shop-item">
								<img class="img-responsive" src="images/product/radar-244-180.jpg" alt="">
								<div class="meta">
									<h4>Radar</h4>
									<p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit,</p>
									<span>Price: <b>$24.5</b></span>
									<a href="?r=product-chart">Add to Cart <i class="fa fa-shopping-cart"></i></a>
								</div>
							</div>
						</div>
						<div class="row best-seller">
							<div class="section-title-style-2">
								<h2>Best Seller</h2>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 single-shop-item">
								<img class="img-responsive" src="images/product/radar-244-180.jpg" alt="">
								<div class="meta">
									<h4>Radar</h4>
									<p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit,</p>
									<span>Price: <b>$24.5</b></span>
									<a href="?r=product-chart">Add to Cart <i class="fa fa-shopping-cart"></i></a>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 single-shop-item">
								<img class="img-responsive" src="images/product/radar-244-180.jpg" alt="">
								<div class="meta">
									<h4>Radar</h4>
									<p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit,</p>
									<span>Price: <b>$24.5</b></span>
									<a href="?r=product-chart">Add to Cart <i class="fa fa-shopping-cart"></i></a>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 single-shop-item">
								<img class="img-responsive" src="images/product/radar-244-180.jpg" alt="">
								<div class="meta">
									<h4>Radar</h4>
									<p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit,</p>
									<span>Price: <b>$24.5</b></span>
									<a href="?r=product-chart">Add to Cart <i class="fa fa-shopping-cart"></i></a>
								</div>
							</div>
						</div>
					</div> <!-- /.shop-page-content -->
					<div class="col-lg-4 col-md-4 col-sm-12 pull-left left_side pdr5"> 
						<h4>Search</h4>
						<form action="#">
							<input type="text" placeholder="Enter Search Keywords">
							<button type="submit"><span class="icon icon-Search"></span></button>
						</form>
						<h4>Categories</h4>
						<ul class="p0 category_item">
							<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Financial Investment</a></li>
							<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Company Growth</a></li>
							<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Taxes and Accounting</a></li>
							<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Financial modeling and planning</a></li>
							<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Profits & Wealth</a></li>
							<li><a href="" class="bottom_item"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Investments in Bonds</a></li>
						</ul>
						<h4>New Products</h4>
						<div class="row single_product_item">
							<div class="col-lg-5">
								<img src="images/product-sidebar/1.jpg" alt="images">
							</div>
							<div class="col-lg-7">
								<p>Neque porro quisqua mest qui dolorem.</p>
								<a href="">more info</a>
							</div>
						</div>
						<div class="row single_product_item">
							<div class="col-lg-5">
								<img src="images/product-sidebar/2.jpg" alt="images">
							</div>
							<div class="col-lg-7">
								<p>Neque porro quisqua mest qui dolorem.</p>
								<a href="">more info</a>
							</div>
						</div>
						<div class="row single_product_item">
							<div class="col-lg-5">
								<img src="images/product-sidebar/3.jpg" alt="images">
							</div>
							<div class="col-lg-7">
								<p>Neque porro quisqua mest qui dolorem.</p>
								<a href="">more info</a>
							</div>
						</div>
						<a href="" class="brochure"><img src="images/product/2.jpg" alt="image"></a>
						<a href="" class="free_shipping"><img src="images/product/3.jpg" alt="image"></a>
					</div> <!-- End left side -->
				</div>  <!-- End row -->
			</div> <!-- End Container -->
		</section>

<!-- ==================shop ================ -->


	</div>
