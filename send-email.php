<?php

/**
 * This example shows how to handle a simple contact form.
 */

$msg = '';
//Don't run this unless we're handling a form submission
if (array_key_exists('email', $_POST)) {
    date_default_timezone_set('Etc/UTC');

    require 'includes/phpmailer/PHPMailerAutoload.php';

    $captcha = "";
    if (isset($_POST["g-recaptcha-response"])) {
        $captcha = $_POST["g-recaptcha-response"];
    }

    if ($captcha != "") {
        
        $secret = "6LdU5iwUAAAAALtqEAsJBfT0Ukb2175bnxfRDPRy";
        $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$captcha."&remoteip=".$_SERVER["REMOTE_ADDR"]), true);

        if ($response["success"] != false) {
            
            //Create a new PHPMailer instance
            $mail = new PHPMailer;
            //Tell PHPMailer to use SMTP - requires a local mail server
            //Faster and safer than using mail()
            $mail->isSMTP();
            $mail->SMTPAuth = true;
            $mail->SMTPDebug = 0;
            $mail->SMTPSecure = 'tls';
            $mail->Host = 'smtp.gmail.com';
            $mail->Port = 587;

            //Use a fixed address in your own domain as the from address
            //**DO NOT** use the submitter's address here as it will be forgery
            //and will cause your messages to fail SPF checks
            $mail->Username = 'rkurniapratama@gmail.com';
            // harus dalam tanda kutip tunggal
            $mail->Password = 'devil123';
            $mail->setFrom('rkurniapratama@gmail.com', 'Modabile');
            //Send the message to yourself, or whoever should receive contact for submissions
            $mail->addAddress('rkurniapratama@gmail.com', 'Rizal Kurnia Pratama');
            $mail->addAddress('rkurniapratama@yahoo.co.id', 'Rizal Kurnia Pratama');
            //Put the submitter's address in a reply-to header
            //This will fail if the address provided is invalid,
            //in which case we should ignore the whole request
            if ($mail->addReplyTo($_POST['email'], $_POST['name'])) {
                $mail->Subject = $_POST['subject'];
                //Keep it simple - don't use HTML
                $mail->isHTML(false);
                //Build a simple message body
                $mail->Body = <<<EOT
Email: {$_POST['email']}
Name: {$_POST['name']}
Message: {$_POST['message']}
EOT;
                //Send the message, check for errors
                if (!$mail->send()) {
                    //The reason for failing to send will be in $mail->ErrorInfo
                    //but you shouldn't display errors to users - process the error, log it on your server.
                    $msg = array('type' => 'error','msg' => $mail->ErrorInfo);
                } else {
                    $msg = array('type' => 'success','msg' => 'Message sent! Thanks for contacting us.');
                }
            } else {
                $msg = array('type' => 'error','msg' => 'Invalid email address, message ignored.');
            }

        } else {
            $msg = array('type' => 'error','msg' => 'Google Recaptcha response error.');
        }
    } else {
        $msg = array('type' => 'error','msg' => 'Google Recaptcha not checked');
    }

    header('Content-Type: application/json');
    echo json_encode($msg);
}


// Define some constants
//define( "RECIPIENT_NAME", "Rizal Kurnia Pratama" );
//define( "RECIPIENT_EMAIL", "rkurniapratama@gmail.com" );

// Read the form values
//$success = false;
//$senderName = isset( $_POST['name'] ) ? preg_replace( "/[^\.\-\' a-zA-Z0-9]/", "", $_POST['name'] ) : "";
//$senderEmail = isset( $_POST['email'] ) ? preg_replace( "/[^\.\-\_\@a-zA-Z0-9]/", "", $_POST['email'] ) : "";
//$subject = isset( $_POST['subject'] ) ? preg_replace( "/[^\.\-\' a-zA-Z0-9]/", "", $_POST['subject'] ) : "";
//$message = isset( $_POST['message'] ) ? preg_replace( "/(From:|To:|BCC:|CC:|Subject:|Content-Type:)/", "", $_POST['message'] ) : "";

// If all values exist, send the email
//if ( $senderName && $senderEmail && $message ) {
//  $recipient = RECIPIENT_NAME . " <" . RECIPIENT_EMAIL . ">";
//  $headers = "From: " . $senderName . " <" . $senderEmail . ">";
//  $success = mail( $recipient, $subject, $message, $headers );
//  echo "<p class='success'>Thanks for contacting us. We will contact you ASAP!</p>";
//}

?>