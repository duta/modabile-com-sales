	<div class="faqs">
<!-- ======= Banner ======= -->
		<section class="p0 container-fluid banner about_banner">
			<div class="about_banner_opacity">
				<div class="container">
					<div class="banner_info_about">
						<h1>Blog Details</h1>
						<ul>
							<li><a href="index.php">Home</a></li>
							<li><i class="fa fa-angle-right"></i></li>
							<li>Blog Details</li>
						</ul>
					</div> <!-- End Banner Info -->
				</div> <!-- End Container -->
			</div> <!-- End Banner_opacity -->
		</section> <!-- End Banner -->
<!-- ================= /Banner ================ -->

<!-- ================== Blog Container ========== -->
		<section class="blog-container two-side-background single-blog-page faqs_sec"> <!-- faqs_sec use for style right side content -->
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 white-left left-content ptb-80">
						<!-- .single-blog-post -->
						<div class="single-blog-post single-page-content anim-5-all">
							<!-- .img-holder -->
							<div class="img-holder">
								<img src="images/blog/14.jpg" alt="">						
							</div><!-- /.img-holder -->
							<div class="post-meta">
								<div class="date-holder">
									<span>09</span> Dec
								</div>
								<div class="title-holder">
									<h2 class="title">Blog heading goes here</h2>
									<ul>
										<li><a href="#">Posted By: John Smith</a></li>
										<li><a href="#">Comments: 2</a></li>
										<li><a href="#"> Categories: Taxes & Accounts</a></li>
									</ul>
								</div>
							</div>
							<div class="content">
								<p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid etx ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>

								<p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid etx ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p> 
								<ul class="list-item style-two dtc">
									<li><i class="fa fa-angle-right"></i> Perspiciatis underpass</li>
									<li><i class="fa fa-angle-right"></i> Omnis iste natus error</li>
									<li><i class="fa fa-angle-right"></i> Voluptatem accusantium</li>
								</ul>
								<ul class="list-item style-two dtc">
									<li><i class="fa fa-angle-right"></i> Perspiciatis underpass</li>
									<li><i class="fa fa-angle-right"></i> Omnis iste natus error</li>
									<li><i class="fa fa-angle-right"></i> Voluptatem accusantium</li>
								</ul>
								<h2>Two Column Text Sample</h2>
								<br>
								<div class="row">
									<div class="col-sm-6">
										<p>Ut enim ad minima veniam, quis nostrum exercita- tio nem ullam corporis suscipit laboriosam, nisi ut aliqu id ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui.</p>
									</div>
									<div class="col-sm-6">
										<p>Ut enim ad minima veniam, quis nostrum exercita- tio nem ullam corporis suscipit laboriosam, nisi ut aliqu id ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui.</p>
									</div>
								</div>
								<br>
								<div class="quote-box dt">
									<i class="fa fa-quote-left dtc"></i>
									<div class="quote-content dtc">
										<p style="margin-bottom: 3px;">Ut enim ad minima veniam, quis nostrum exercitatio nem ullam corporis suscipit labori osam, nisi ut aliqu id ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur.</p>
										<span class="name">
											- Michale John
										</span>
									</div>
								</div>
								<br> <br>
								<p><b style="color: #8e8d8d;font-style: italic;">Here is main text</b> quis nostrud exercitation ullamco laboris nisi here is itealic text ut aliquip ex ea comm- odo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat <a href="">here is link text</a> cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
							</div>
							
							<!-- .author-box -->
							<div class="author-box">
								<!-- .top-author-info -->
								<div class="top-author-info dt">
									<!-- .img-holder -->
									<div class="img-holder dtc">
										<img src="images/blog/15.jpg" alt="image">
									</div><!-- /.img-holder -->
									<div class="top-author-info-content dtc">
										<h4>administrator <span>(Michale John)</span></h4>
										<p>Ut enim ad minima veniam, quis nostrum exercitatio nem ullam corporis susc ipit laboriosam, nisi ut aliqu id ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit.</p>
									</div>
								</div><!-- /.top-author-info -->
								<div class="bottom-author-info clearfix">
									<div class="submitted-post">
										<span>Total 12 Posts </span>
									</div>
									<div class="social-icons">
										<ul>
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
											<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
											<li><a href="#"><i class="fa fa-pinterest"></i></a></li>
											<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
										</ul>
									</div>
								</div>
							</div><!-- /.author-box -->

							<div class="comment-box">
								<h3>Comments <span>4</span></h3>
								<!-- .comment-holder -->
								<div class="comment-holder">
									<!-- .single-comment -->
									<div class="single-comment">
										<!-- .img-holder -->
										<div class="img-holder">
											<img src="images/blog/16.jpg" alt="images">
										</div><!-- /.img-holder -->
										<div class="content">
											<h4>Merry John</h4>
											<p>Duis aute irure dolor in reprehenderit in vol uptate velit esse cillum dolore eu fugiat nulla pari atur. Excepteur sint occaecat cupidatat non proid pent.</p>
											<ul class="meta">
												<li><a href="#" class="date">Dec 09 2015</a></li>
												<li><a href="#" class="reply">Reply</a></li>
											</ul>
										</div>
									</div><!-- /.single-comment -->
									<!-- .single-comment -->
									<div class="single-comment">
										<!-- .img-holder -->
										<div class="img-holder">
											<img src="images/blog/17.jpg" alt="images">
										</div><!-- /.img-holder -->
										<div class="content">
											<h4>Merry John</h4>
											<p>Duis aute irure dolor in reprehenderit in vol uptate velit esse cillum dolore eu fugiat nulla pari atur. Excepteur sint occaecat cupidatat non proid pent.</p>
											<ul class="meta">
												<li><a href="#" class="date">Dec 09 2015</a></li>
												<li><a href="#" class="reply">Reply</a></li>
											</ul>
										</div>
									</div><!-- /.single-comment -->
									<!-- .single-comment -->
									<div class="single-comment">
										<!-- .img-holder -->
										<div class="img-holder">
											<img src="images/blog/18.jpg" alt="images">
										</div><!-- /.img-holder -->
										<div class="content">
											<h4>Merry John</h4>
											<p>Duis aute irure dolor in reprehenderit in vol uptate velit esse cillum dolore eu fugiat nulla pari atur. Excepteur sint occaecat cupidatat non proid pent.</p>
											<ul class="meta">
												<li><a href="#" class="date">Dec 09 2015</a></li>
												<li><a href="#" class="reply">Reply</a></li>
											</ul>
										</div>
									</div><!-- /.single-comment -->
									<!-- .single-comment -->
									<div class="single-comment">
										<!-- .img-holder -->
										<div class="img-holder">
											<img src="images/blog/16.jpg" alt="images">
										</div><!-- /.img-holder -->
										<div class="content">
											<h4>Merry John</h4>
											<p>Duis aute irure dolor in reprehenderit in vol uptate velit esse cillum dolore eu fugiat nulla pari atur. Excepteur sint occaecat cupidatat non proid pent.</p>
											<ul class="meta">
												<li><a href="#" class="date">Dec 09 2015</a></li>
												<li><a href="#" class="reply">Reply</a></li>
											</ul>
										</div>
									</div><!-- /.single-comment -->
								</div><!-- /.comment-holder -->
							</div>
							<!-- .comment-form -->
							<div class="comment-form">
								<h3>Leave a Comment</h3>
								<form action="#">
									<div class="form-group">
										<p class="half">
											<input type="text" placeholder="Enter your name">
										</p>
										<p class="half">
											<input type="text" placeholder="Enter your email address">
										</p>
									</div>
									<p><textarea placeholder="Wrtie message"></textarea></p>
									<button type="submit" class="submit">submit now <i class="fa fa-arrow-circle-right"></i></button>
								</form>
							</div><!-- /.comment-form -->
						</div><!-- /.single-blog-post -->
					</div>

					<div class="col-lg-4 col-md-4 col-sm-12 pull-left left_side pdr5"> <!--for this page-(Right Side) -->
						<h4>Search</h4>
						<form action="#">
							<input type="text" placeholder="Enter Search Keywords">
							<button type="submit"><span class="icon icon-Search"></span></button>
						</form>
						<h4>About Us</h4>
						<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipis civelit sed quia non qui dolorem ipsum quia dolor sit amet, consectetur, adipis civelit sed quia numquam eius modi.</p>
						<h4>Categories</h4>
						<ul class="p0 category_item">
							<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Financial Investment</a></li>
							<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Company Growth</a></li>
							<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Taxes and Accounting</a></li>
							<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Financial modeling and planning</a></li>
							<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Profits & Wealth</a></li>
							<li><a href="" class="bottom_item"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Investments in Bonds</a></li>
						</ul>
						<h4>Popular Posts</h4>
						<ul class="p0 post_item">
							<li>AUG 12,2015<a href="">Making Cents Investments in Start-ups become profitable for Companies ...</a></li>
							<li>AUG 12,2015<a href="">Making Cents Investments in Start-ups become profitable for Companies ...</a></li>
							<li>AUG 12,2015<a href="" class="bottom_item">Making Cents Investments in Start-ups become profitable for Companies ...</a></li>
						</ul>
						<h4>Meet Our Advisior</h4>
						<a class="img_holder" href=""><img class="img-responsive" src="images/service/11.jpg" alt="image"></a>
						<h5><a href="">Merry Michale</a></h5>
						<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipis civelit sed quia non qui dolorem</p>
						<h4>Tags Clouds</h4>
						<ul class="p0 clouds">
							<li><a href="">financial investiment</a></li>
							<li><a href="">Taxes</a></li>
							<li><a href="">Account & Profit</a></li>
							<li><a href="">Insurances</a></li>
							<li><a href="">Real news</a></li>
							<li><a href="">OUr advisors</a></li>
						</ul>
					</div> <!-- End left side -->
				</div>
			</div>
		</section>
<!-- ================== /Blog Container ========== -->
	</div>