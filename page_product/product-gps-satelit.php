	<div class="shop">

<!-- ======= Banner ======= -->
		<section class="p0 container-fluid banner about_banner">
			<div class="about_banner_opacity">
				<div class="container">
					<div class="banner_info_about">
						<h1>Data GPS Berbasis Satelit</h1>
						<ul>
							<li>Produk</li>
                            <li><i class="fa fa-angle-right"></i></li>
                            <li>Data GPS Berbasis Satelit</li>
						</ul>
						
					</div> <!-- End Banner Info -->
				</div> <!-- End Container -->
			</div> <!-- End Banner_opacity -->
		</section> <!-- End Banner -->
<!-- ======= /Banner ======= -->
<!-- ==================shop ================ -->
		<section class="post-content"> <!-- faqs_sec use for style left side content -->
			<div class="container">
				<div class="row">
							<div class="col-md-4">
								<div class="polaroid">
								<img src="images/product/product-gps-satelit.jpg" width="100%">
									<div class="polaroid-text">
										<h2>GPS Berbasis Satelit</h2>
									</div>
								</div>
							</div>
							<div class="col-md-8">
								<div class="polaroid-text">
								<h2>Keterangan</h2>
									<p>Pengguna produk dan layanan ini umumnya perusahaan pengelola armada kendaraan/kapal dengan lintasan jarak jauh, antar pulau antar negara. Dengan data GPS Satelit pemilik kapal mengharuskan memasang perangkat tambahan GPS Satelit dan berlangganan data posisi Kapal secara bulanan. sedangkan untuk memantau posisi kapal bisa menggunakan Software Modabile yang ada di Server modabile maupun di server pemilik kapal.
									Kelebihan menggunakan data GPS berbasis satelit adalah update posisi kapal bisa secara kontinue setiap 1 jam.
									</p>
								</div>
							</div>
				</div>  <!-- End row -->
			</div> <!-- End Container -->
		</section>

<!-- ==================shop ================ -->


	</div>
