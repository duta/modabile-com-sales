	<div class="shop">

<!-- ======= Banner ======= -->
		<section class="p0 container-fluid banner about_banner">
			<div class="about_banner_opacity">
				<div class="container">
					<div class="banner_info_about">
						<h1>Data GPS Berbasis Wifi</h1>
						<ul>
							<li>Produk</li>
                            <li><i class="fa fa-angle-right"></i></li>
                            <li>Data GPS Berbasis Wifi</li>
						</ul>
						
					</div> <!-- End Banner Info -->
				</div> <!-- End Container -->
			</div> <!-- End Banner_opacity -->
		</section> <!-- End Banner -->
<!-- ======= /Banner ======= -->
<!-- ==================shop ================ -->
		<section class="post-content"> <!-- faqs_sec use for style left side content -->
			<div class="container">
				<div class="row">
							<div class="col-md-4">
								<div class="polaroid">
								<img src="images/product/product-gps-wifi.jpg" width="100%">
									<div class="polaroid-text">
										<h2>GPS Berbasis Wifi</h2>
									</div>
								</div>
							</div>
							<div class="col-md-8">
									<div class="polaroid-text">
									<h2>Keterangan</h2>
										<p>Pengguna produk dan layanan ini umumnya perusahaan pengelola armada kendaraan dalam area tertentu yang tercover oleh jaringan radio WIFI, sehingga untuk update posisi kendaraan menggunakan jaringan wifi yang tesedia. 
										Kelebihan mengunakan teknologi wifi ini adalah tidak ada biaya langganan data secara bulanan, namun ada perangkat kusus yang dipasang dikendaraan.
										</p>
									</div>
							</div>
				</div>  <!-- End row -->
			</div> <!-- End Container -->
		</section>

<!-- ==================shop ================ -->


	</div>
