	<div class="shop">

<!-- ======= Banner ======= -->
		<section class="p0 container-fluid banner about_banner">
			<div class="about_banner_opacity">
				<div class="container">
					<div class="banner_info_about">
						<h1>Data GPS Berbasis GSM</h1>
						<ul>
							<li>Produk</li>
                            <li><i class="fa fa-angle-right"></i></li>
                            <li>Data GPS Berbasis GSM</li>
						</ul>
						
					</div> <!-- End Banner Info -->
				</div> <!-- End Container -->
			</div> <!-- End Banner_opacity -->
		</section> <!-- End Banner -->
<!-- ======= /Banner ======= -->
<!-- ==================shop ================ -->
		<section class="post-content"> <!-- faqs_sec use for style left side content -->
			<div class="container">
				<div class="row">
							<div class="col-md-4">
								<div class="polaroid">
								<img src="images/product/product-gps-gsm.jpg" width="100%">
									<div class="polaroid-text">
										<h2>GPS Berbasis GSM</h2>
									</div>
								</div>
							</div>
							<div class="col-md-8">
									<div class="polaroid-text">
									<h2>Keterangan</h2>
										<p>Pengguna produk dan Layanan ini umumnya perusahaan pengelola armada kendaraan pribadi, truk dan kapal yang masih terjangkau oleh sinyal GSM. Sehingga untuk update posisi kapal menggunakan sinyal GPRS atau SMS.
										Namun perangkat ini tidak cocok untuk armada dengan lintasan Jarak jauh ( antar pulau antar negara), kelebihan perangkat ini langganan data bulan relatif murah dari pada menggunakan satelit.
										</p>
									</div>
							</div>
				</div>  <!-- End row -->
			</div> <!-- End Container -->
		</section>

<!-- ==================shop ================ -->


	</div>
