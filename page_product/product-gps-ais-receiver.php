	<div class="shop">

<!-- ======= Banner ======= -->
		<section class="p0 container-fluid banner about_banner">
			<div class="about_banner_opacity">
				<div class="container">
					<div class="banner_info_about">
						<h1>Data GPS Berbasis AIS Receiver</h1>
						<ul>
							<li>Produk</li>
                            <li><i class="fa fa-angle-right"></i></li>
                            <li>Data GPS Berbasis AIS Receiver</li>
						</ul>
						
					</div> <!-- End Banner Info -->
				</div> <!-- End Container -->
			</div> <!-- End Banner_opacity -->
		</section> <!-- End Banner -->
<!-- ======= /Banner ======= -->
<!-- ==================shop ================ -->
		<section class="post-content"> <!-- faqs_sec use for style left side content -->
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="polaroid">
							<img src="images/product/gps-ais-receiver.jpg" width="100%">
							<img src="images/product/gps-ais-receiver2.jpg" width="100%">
							<div class="polaroid-text">
								<h2>GPS Berbasis AIS Satelit</h2>
							</div>
						</div>
					</div>
							<div class="col-md-8">
									<div class="polaroid-text">
									<h2>Keterangan</h2>
											<p>Pengguna produk dan layanan ini umumnya adalah perusahaan pengelola area pelabuhan, jadi kebutuhannya adalah memantau dan monitoring pergerakan kapal daalam area tertentu ( coverage AIS Receiver).  Ada perangkat AIS receiver yang harus di pasang dan Sofware modabila yang di instal di server perusahaan.
											Kelebihan Produk dan Layanan ini adalah data posisi kapal yang ditampilkan bisa mendekati real time, karena langsung menerima broadcast posisi kapal yang menggunakan AIS di area tertentu, namun kelemahan dari produk dan layanan ini tidak bisa menjangka untuk posisi kapal jarak jauh ( diluar coverage AIS Receiver).
											</p>
									</div>
							</div>
				</div>  <!-- End row -->
			</div> <!-- End Container -->
		</section>

<!-- ==================shop ================ -->


	</div>
