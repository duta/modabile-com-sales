	<div class="shop">

<!-- ======= Banner ======= -->
		<section class="p0 container-fluid banner about_banner">
			<div class="about_banner_opacity">
				<div class="container">
					<div class="banner_info_about">
						<h1>Data GPS Berbasis AIS Satelit</h1>
						<ul>
							<li>Produk</li>
                            <li><i class="fa fa-angle-right"></i></li>
                            <li>Data GPS Berbasis AIS Satelit</li>
						</ul>
						
					</div> <!-- End Banner Info -->
				</div> <!-- End Container -->
			</div> <!-- End Banner_opacity -->
		</section> <!-- End Banner -->
<!-- ======= /Banner ======= -->
<!-- ==================shop ================ -->
		<section class="post-content"> <!-- faqs_sec use for style left side content -->
			<div class="container">
				<div class="row">
							<div class="col-md-4">
								<div class="polaroid">
								<img src="images/product/product-gps-ais-satelit.jpg" width="100%">
									<div class="polaroid-text">
										<h2>GPS Berbasis AIS Satelit</h2>
									</div>
								</div>
							</div>
							<div class="col-md-8">
									<div class="polaroid-text">
									<h2>Keterangan</h2>
										<p>Pengguna produk dan layanan ini umumnya perusahaan pengelola armada kapal dengan lintasan jarak jauh, Dengan data AIS Satelit pemilik kapal hanya berlangganan layanan data AiS Satelit secara bulanan tidak memerlukan perangkat tambahan namun , sedangkan untuk memantau posisi kapal bisa menggunakan Software Modabile yang ada di Server modabile maupun di server pemilik kapal. Kelebihan produk dan layanan ini tidak memerlukan perangkat tambahan hanya berlangganan data AIS Satelit dengan kami. Kondisi yang perlu diperhatikan dengan produk dan layanan ini adalah update data posisi kapal range paling cepat 1 jam dan paling lambat 6 jam.</p>
									</div>
							</div>
				</div>  <!-- End row -->
			</div> <!-- End Container -->
		</section>

<!-- ==================shop ================ -->


	</div>
