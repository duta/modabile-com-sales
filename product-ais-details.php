<div class="product_page">
<!-- ======= Banner ======= -->
		<section class="p0 container-fluid banner about_banner">
			<div class="about_banner_opacity">
				<div class="container">
					<div class="banner_info_about">
						<h1>Data AIS Receiver</h1>
						<ul>
							<li><a href="index.php">Home</a></li>
							<li><i class="fa fa-angle-right"></i></li>
							<li>Product</li>
							<li><i class="fa fa-angle-right"></i></li>
                            				<li>Data AIS Receiver</li>
						</ul>
					</div> <!-- End Banner Info -->
				</div> <!-- End Container -->
			</div> <!-- End Banner_opacity -->
		</section> <!-- End Banner -->
<!-- ================= /Banner ================ -->

<!-- =================== Product Details============ -->
		<section class="shop_container product_details_container faqs_sec"> <!-- faqs_sec use for style left side content -->
			<div class="container">
				<div class="row">
					<!-- .product-details-page-content -->
					<div class="white_bg right_side col-lg-8 col-md-8 col-sm-12 col-xs-12">
						<div>
							<div class="float_left">
								<p>Produk ini sangat bermanfaat bagi perusahaan pengelola jasa Pelabuhan atau otoritas pelabuhan untuk memantau pergerakan Kapal di Area Tertentu dengan memasang perangkat AIS Receiver.Produk ini bisa memanfaatkan layanan Aplikasi yang terintall di Server modabile atau terinstal di Server Pelanggan.</p>
							</div>
						</div>
					</div> <!-- /.product-details-page-content -->
				</div>
			</div>
		</section>

<!-- =================== Product Details============ -->
</div>		
